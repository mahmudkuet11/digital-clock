var Clock = {
    classMapsForDigits: {
        '0': ['top', 'top_right', 'bottom_right', 'bottom', 'bottom_left', 'top_left'],
        '1': ['top_right', 'bottom_right'],
        '2': ['top', 'top_right', 'middle', 'bottom_left', 'bottom'],
        '3': ['top', 'top_right', 'middle', 'bottom_right', 'bottom'],
        '4': ['top_left', 'middle', 'top_right', 'bottom_right'],
        '5': ['top', 'top_left', 'middle', 'bottom_right', 'bottom'],
        '6': ['top', 'top_left', 'middle', 'bottom_right', 'bottom', 'bottom_left'],
        '7': ['top', 'top_right', 'bottom_right'],
        '8': ['top', 'top_right', 'middle', 'bottom_right', 'bottom', 'bottom_left', 'top_left'],
        '9': ['top', 'top_right', 'middle', 'bottom_right', 'bottom', 'top_left'],
    },
    init: function () {
        this.showDigitForElement(document.querySelector('.digit'), 9);

        this.listen();
    },
    listen: function () {
        setInterval(function () {
            var date = new Date();
            var hours = this.makeDoubleDigit(date.getHours());
            var minutes = this.makeDoubleDigit(date.getMinutes());
            var seconds = this.makeDoubleDigit(date.getSeconds());

            this.showDigitForElement(document.querySelector('.digit.hour_1'), hours[0]);
            this.showDigitForElement(document.querySelector('.digit.hour_2'), hours[1]);

            this.showDigitForElement(document.querySelector('.digit.minute_1'), minutes[0]);
            this.showDigitForElement(document.querySelector('.digit.minute_2'), minutes[1]);

            this.showDigitForElement(document.querySelector('.digit.second_1'), seconds[0]);
            this.showDigitForElement(document.querySelector('.digit.second_2'), seconds[1]);

        }.bind(this), 1000);
    },
    showDigitForElement: function (el, digit) {
        var bars = el.querySelectorAll('.bar');
        for(var i=0; i<bars.length; i++){
            bars[i].classList.add(['dimmed']);
        }
        var classes = this.classMapsForDigits[digit];
        for(var i=0; i<classes.length; i++){
            el.querySelector('.bar.' + classes[i]).classList.remove(['dimmed']);
        }
    },
    makeDoubleDigit: function (digit) {
        if (digit < 10) {
            return '0' + digit;
        }
        return digit + '';
    }
};

Clock.init();
